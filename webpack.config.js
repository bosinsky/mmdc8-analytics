var webpack = require('webpack');
var path = require('path');
var libraryName = 'mmdc8';
var outputFile = libraryName + '.js';

var config = {
  entry: __dirname + '/index.js',
  output: {
    path: __dirname + '/lib',
    filename: outputFile,
    library: libraryName
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets:[ 'es2015', 'stage-2' ]
        }
      }
    ]
  }
};

module.exports = config;