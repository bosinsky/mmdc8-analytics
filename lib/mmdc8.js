var mmdc8 =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Serializable = exports.Serializable = function () {
    function Serializable(props) {
        _classCallCheck(this, Serializable);

        this.properties = props || {};
    }

    _createClass(Serializable, [{
        key: 'toObject',
        value: function toObject() {
            return this.properties;
        }
    }, {
        key: 'toString',
        value: function toString() {
            return JSON.stringify(this.toObject());
        }
    }, {
        key: 'toJSON',
        value: function toJSON() {
            return JSON.stringify(this.properties);
        }
    }, {
        key: 'toQueryString',
        value: function toQueryString() {
            var str = [];
            var obj = this.toObject();
            for (var p in obj) {
                if (obj.hasOwnProperty(p) && obj[p]) {
                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                }
            }
            return str.join('&');
        }
    }]);

    return Serializable;
}();

var Hit = function (_Serializable) {
    _inherits(Hit, _Serializable);

    function Hit(props) {
        _classCallCheck(this, Hit);

        var _this = _possibleConstructorReturn(this, (Hit.__proto__ || Object.getPrototypeOf(Hit)).call(this, props));

        _this.send = false;
        return _this;
    }

    return Hit;
}(Serializable);

var PageHit = exports.PageHit = function (_Hit) {
    _inherits(PageHit, _Hit);

    function PageHit(screenName) {
        _classCallCheck(this, PageHit);

        return _possibleConstructorReturn(this, (PageHit.__proto__ || Object.getPrototypeOf(PageHit)).call(this, { dp: screenName, t: 'pageview' }));
    }

    return PageHit;
}(Hit);

var ScreenHit = exports.ScreenHit = function (_Hit2) {
    _inherits(ScreenHit, _Hit2);

    function ScreenHit(screenName) {
        _classCallCheck(this, ScreenHit);

        return _possibleConstructorReturn(this, (ScreenHit.__proto__ || Object.getPrototypeOf(ScreenHit)).call(this, { cd: screenName, t: 'screenview' }));
    }

    return ScreenHit;
}(Hit);

var Event = exports.Event = function (_Hit3) {
    _inherits(Event, _Hit3);

    function Event(category, action, label, value) {
        _classCallCheck(this, Event);

        return _possibleConstructorReturn(this, (Event.__proto__ || Object.getPrototypeOf(Event)).call(this, { ec: category, ea: action, el: label, ev: value, t: 'event' }));
    }

    return Event;
}(Hit);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Analytics = exports.Event = exports.PageHit = exports.ScreenHit = undefined;

var _hits = __webpack_require__(0);

var _analytics = __webpack_require__(2);

var _analytics2 = _interopRequireDefault(_analytics);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.ScreenHit = _hits.ScreenHit;
exports.PageHit = _hits.PageHit;
exports.Event = _hits.Event;
exports.Analytics = _analytics2.default;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _hits = __webpack_require__(0);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var defaultOptions = { debug: false };

var Analytics = function () {
    function Analytics(propertyId) {
        var additionalParameters = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultOptions;

        _classCallCheck(this, Analytics);

        this.queue = [];
        this.customDimensions = [];
        this.propertyId = propertyId;
        this.options = options;
        this.options = options;
        this.clientId = 'mmdc8';
        this.userAgent = 'mmdc8';
        this.parameters = _extends({
            an: packageJson.name,
            aid: 'mmdc8',
            av: packageJson.version
        }, additionalParameters);
        this.ready = true;
        this.flush();
    }

    _createClass(Analytics, [{
        key: 'hit',
        value: function hit(_hit) {
            this.queue.push(_hit);
            this.flush();
        }
    }, {
        key: 'event',
        value: function event(_event) {
            this.queue.push(_event);
            this.flush();
        }
    }, {
        key: 'addCustomDimension',
        value: function addCustomDimension(index, value) {
            this.customDimensions[index] = value;
        }
    }, {
        key: 'removeCustomDimension',
        value: function removeCustomDimension(index) {
            delete this.customDimensions[index];
        }
    }, {
        key: 'flush',
        value: function flush() {
            var _this = this;

            if (this.ready) {
                var _loop = function _loop() {
                    var hit = _this.queue.pop();
                    _this.send(hit).then(function () {
                        return hit.sent = true;
                    });
                };

                while (this.queue.length) {
                    _loop();
                }
            }
        }
    }, {
        key: 'send',
        value: function send(hit) {
            /* format: https://www.google-analytics.com/collect? +
            * &tid= GA property ID (required)
            * &v= GA protocol version (always 1) (required)
            * &t= hit type (pageview / screenview)
            * &dp= page name (if hit type is pageview)
            * &cd= screen name (if hit type is screenview)
            * &cid= anonymous client ID (optional if uid is given)
            * &uid= user id (optional if cid is given)
            * &ua= user agent override
            * &an= app name (required for any of the other app parameters to work)
            * &aid= app id
            * &av= app version
            * &sr= screen resolution
            * &cd{n}= custom dimensions
            * &z= cache buster (prevent browsers from caching GET requests -- should always be last)
            */

            var customDimensions = this.customDimensions.map(function (value, index) {
                return 'cd' + index + '=' + value;
            }).join('&');

            var params = new _hits.Serializable(this.parameters).toQueryString();

            var url = 'https://www.google-analytics.com/collect?tid=' + this.propertyId + '&v=1&cid=' + this.clientId + '&' + hit.toQueryString() + '&' + params + '&' + customDimensions + '&z=' + Math.round(Math.random() * 1e8);

            var options = {
                method: 'get',
                headers: {
                    'User-Agent': this.userAgent
                }
            };

            return fetch(url, options);
        }
    }]);

    return Analytics;
}();

exports.default = Analytics;

/***/ })
/******/ ]);